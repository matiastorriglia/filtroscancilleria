﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public class FilterSelect : MonoBehaviour
{
    public CanvasGroup[] filters = new CanvasGroup[3];
    public CanvasGroup previewCG;
    public Animator selectAnim;
    public Animator pictureAnim;
    public Animator selectHideAnim;
    public Transform selectHideButton;

    public RawImage preview;

    byte[] bytes;

    public CanvasGroup pictureButtonCG;
    string image_url;

    bool hiding = false;

    private void Start()
    {
        Screen.fullScreen = true;
    }

    public void GoToSampleScene()
    {
        SceneManager.LoadScene("DeviceCameraWebGL");
    }

    public void ShowFilter(int f)
    {
        for(int i = 0; i < filters.Length; i++)
        {
            if (i == f)
                filters[i].alpha = 1;
            else
                filters[i].alpha = 0;
        }

        pictureButtonCG.alpha = 1;
        pictureButtonCG.interactable = pictureButtonCG.blocksRaycasts = true;
    }

    public void TakePicture()
    {
        selectAnim.Play("selectoranim", -1, 0);
        selectHideAnim.Play("selectoranim", -1, 0);
        StartCoroutine(TakePictureCR());
    }

    public void HideSelect()
    {
        if (!hiding)
        {
            hiding = true;
            selectAnim.Play("selectoranim", -1, 0);
            selectHideButton.localScale = new Vector3(-1, 1, 1);
        }else {
            hiding = false;
            selectAnim.Play("selectoranim 0", -1, 0);
            selectHideButton.localScale = new Vector3(1, 1, 1);
        }
    }

    IEnumerator TakePictureCR()
    {
        pictureAnim.Play("countdownanim", -1, 0);
        yield return new WaitForSeconds(2.16f);
        StartCoroutine(UploadPNG());

    }

    IEnumerator UploadPNG()
    {
        // We should only read the screen after all rendering is complete
        yield return new WaitForEndOfFrame();

        preview.texture = null;

        // Create a texture the size of the screen, RGB24 format
        int width = Screen.width;
        int height = Screen.height;
        var tex = new Texture2D(width, height, TextureFormat.RGB24, false);
        var screenshotTex = new Texture2D(width, height, TextureFormat.PVRTC_RGBA4, false);

        // Read screen contents into the texture
        tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        tex.Apply();

        // Encode texture into PNG
        bytes = tex.EncodeToPNG();

        //string ToBase64String byte[]
        string encodedText = System.Convert.ToBase64String(bytes);

        image_url = "data:image/png;base64," + encodedText;

        Debug.Log(image_url);
        preview.texture = tex;

        previewCG.alpha = 1;
        previewCG.interactable = previewCG.blocksRaycasts = true;
    }

    public void CancelPicture()
    {
        hiding = false;
        selectAnim.Play("selectoranim 0", -1, 0);
        selectHideAnim.Play("selectoranim 0", -1, 0);
        selectHideButton.localScale = new Vector3(1, 1, 1);
        previewCG.alpha = 0;
        previewCG.interactable = previewCG.blocksRaycasts = false;
        bytes = new byte[0];
    }

    public void DownloadPicture()
    {
#if !UNITY_EDITOR
        openWindow(image_url);
#endif
        selectAnim.Play("selectoranim 0", -1, 0);
        selectHideAnim.Play("selectoranim 0", -1, 0);
        previewCG.alpha = 0;
        previewCG.interactable = previewCG.blocksRaycasts = false;
        bytes = new byte[0];
    }

    [DllImport("__Internal")]
    private static extern void openWindow(string url);
}
